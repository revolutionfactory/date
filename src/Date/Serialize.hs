module Date.Serialize where

import Date.Types
import Data.Function ((&))
import Control.Newtype (unpack)


class Serialize a where
  serialize :: a -> ShowS

instance Serialize Date where serialize (Date {year,month,day}) = serialize year . serialize month . serialize day
instance Serialize Year where serialize = leftPad 4 '0' . show . unpack
instance Serialize Month where serialize = leftPad 2 '0' . show . (+1) . fromEnum
instance Serialize Day where serialize = leftPad 2 '0' . show . unpack

render :: Date -> ShowS
render (Date {year, month, day}) = 
  leftPad 4 '0' (year & toInteger >>> show) .
  leftPad 2 '0' (month & fromEnum >>> (+ 1) >>> show) .
  leftPad 2 '0' (day & unpack >>> show)


leftPad :: Int -> Char -> String -> ShowS
leftPad n c s = lp (n - l) . showString s
  where
  l = length s
  lp n' xs | n' > 0 = c : lp (n' - 1) xs
           | otherwise = xs

