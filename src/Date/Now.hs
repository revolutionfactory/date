module Date.Now where

import Control.Monad.IO.Class (MonadIO(liftIO))
import System.Process.Typed (readProcess_, proc)
import Date.Parse
import Date.Types
import Control.Monad.Fail (MonadFail)


now :: (MonadIO m, MonadFail m) => m Date
now = do
  (dateString,_) <- liftIO $ readProcess_ $ proc "date" ["+%Y %m %d"]
  DateSpace d <- parse dateString
  pure d

newtype DateSpace = DateSpace Date deriving (Show, Eq)

instance Parsec DateSpace where
  parsec = do
    year <- parsec
    skipSpaces
    month <- parsec
    skipSpaces
    day <- parsec
    pure $ DateSpace $ Date {..}

