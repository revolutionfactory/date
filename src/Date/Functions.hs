{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
module Date.Functions where

import Data.Coerce (Coercible, coerce)
import Date.Types
import Data.Function ((&))


class GetMonth a where getMonth :: a -> Month
instance GetMonth (DateX a) where getMonth = month
instance GetMonth Month where getMonth = id
instance GetMonth (a,Month) where getMonth = snd

class GetYear  a where getYear  :: a -> Year
instance GetYear (DateX a) where getYear = year
instance GetYear (Year, a) where getYear = fst
instance GetYear Year where getYear = id

daysInMonth :: (GetYear a, GetMonth a, Num b) => a -> b
daysInMonth d = case getMonth d of
  January   -> 31 
  February  -> if isLeapYear d then 29 else 28 -- 28 days in a common year and 29 days in leap years
  March     -> 31 
  April     -> 30 
  May       -> 31 
  June      -> 30 
  July      -> 31 
  August    -> 31 
  September -> 30 
  October   -> 31 
  November  -> 30 
  December  -> 31 

isLeapYear :: GetYear a => a -> Bool
isLeapYear = getYear >>> \year -> (divisableBy four year && not (divisableBy oneHundred year)) || divisableBy fourHundred year

newtype Divisor = Divisor Integer

divisableBy :: (Integral a, Coercible a Integer) => Divisor -> a -> Bool
divisableBy (Divisor d) = (== 0) . (`mod` coerce d)

fourHundred :: Divisor
fourHundred = Divisor 400

oneHundred :: Divisor
oneHundred = Divisor 100

four :: Divisor
four = Divisor 4

addDays :: Integer -> Date -> Date
addDays x d = fromInteger <$> normalizeDate ((x +) . toInteger <$> d)

normalizeDate :: DateX Integer -> DateX Integer
normalizeDate d
  | day d < 1   = normalizeDate (decrementMonth d & \d -> d {day = day d + daysInMonth d})
  | day d > dim = normalizeDate (incrementMonth d & \d -> d {day = day d - dim})
  | otherwise   = d
  where dim = daysInMonth d

incrementMonth :: DateX a -> DateX a
incrementMonth (Date {year, month = cycleNext -> month, day})
  | month == minBound = Date {year = year + 1, month, day}
  | otherwise         = Date {year,            month, day}

decrementMonth :: DateX a -> DateX a
decrementMonth (Date {year, month = cyclePrev -> month, day})
  | month == maxBound = Date {year = year - 1, month, day}
  | otherwise         = Date {year,            month, day}

cycleNext :: (Enum a, Eq a, Bounded a) => a -> a
cycleNext x
  | x == maxBound = minBound
  | otherwise     = succ x

cyclePrev :: (Enum a, Eq a, Bounded a) => a -> a
cyclePrev x
  | x == minBound = maxBound
  | otherwise     = pred x

