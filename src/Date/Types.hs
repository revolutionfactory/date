module Date.Types where

import Control.Newtype (Newtype(unpack))
import Numeric.Natural (Natural)
import GHC.Generics (Generic)


type Date = DateX Natural

data DateX a = Date
  { year  :: Year
  , month :: Month
  , day   :: DayX a
  } deriving (Show, Eq, Ord, Functor, Generic)

newtype Year = Year Integer deriving (Show, Eq, Ord, Num, Enum, Integral, Real, Read)
instance Newtype Year Integer

data Month
  = January   -- 31 days
  | February  -- 28 days in a common year and 29 days in leap years
  | March     -- 31 days
  | April     -- 30 days
  | May       -- 31 days
  | June      -- 30 days
  | July      -- 31 days
  | August    -- 31 days
  | September -- 30 days
  | October   -- 31 days
  | November  -- 30 days
  | December  -- 31 days
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

type Day = DayX Natural
newtype DayX a = Day a deriving (Eq, Ord, Num, Enum, Read, Functor, Generic)
instance Newtype (DayX a) a
instance Show a => Show (DayX a) where show = show . unpack

(>>>) :: (a -> b) -> (b -> c) -> a -> c
(>>>) = flip (.)

