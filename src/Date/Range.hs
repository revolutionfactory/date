module Date.Range where

import Date.Types (Date)

data DateRange = DateRange
  { startDate :: Date
  , endDate :: Date
  }

