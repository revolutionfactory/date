module Date.Parse where

import Control.Applicative (liftA3)
import Control.Monad.Fail (MonadFail(fail))
import Control.Monad.State (MonadState(get,put), evalStateT)
import Data.Bifunctor (first)
import Data.MonoTraversable (MonoFoldable(ofoldr), Element)
import Data.Proxy (Proxy(Proxy))
import Data.Sequences (uncons, IsSequence)
import Data.Typeable (Typeable, showsTypeRep, typeRep)
import Data.Word (Word8)
import Date.Types
import Prelude hiding (take, fail, read)
import Safe (readMay)
import Text.Read (Read)


parse
  :: forall a s m
  .( Parsec a
   , IsSequence s
   , MonadFail m
   , IsChar (Element s)
   , ToChar (Element s)
   , Eq (Element s)
   )
  => s -> m a
parse = evalStateT parsec

newtype Parse a = Parse (Either String a) deriving (Show, Eq, Functor, Applicative, Monad)
instance MonadFail Parse where fail = Parse . Left

class Parsec a where
  parsec ::
     ( IsSequence s
     , MonoFoldable s
     , MonadState s m
     , MonadFail m
     , IsChar (Element s)
     , ToChar (Element s)
     , Eq (Element s)
     )
    => m a

class IsChar a where fromChar :: Char -> a
instance IsChar Char where fromChar = id
instance IsChar Word8 where fromChar = toEnum . fromEnum

class ToChar a where toChar :: a -> Char
instance ToChar Char where toChar = id
instance ToChar Word8 where toChar = toEnum . fromEnum

instance Parsec Date where parsec = liftA3 Date parsec parsec parsec
instance Parsec Year where parsec = take 4 >>= read >>> fmap Year
instance Parsec Month where parsec = take 2 >>= read >>= (subtract 1) >>> toEnum >>> pure
instance Parsec Day where parsec = take 2 >>= read >>> fmap Day

skipSpaces :: (IsSequence s, IsChar (Element s), Eq (Element s), MonadState s m) => m ()
skipSpaces = skipWhile $ (== fromChar ' ')

skipWhile :: (IsSequence s, MonadState s m) => (Element s -> Bool) -> m ()
skipWhile f = get >>= uncons >>> fmap (first f) >>> \case
  Just (True, xs) -> put xs
  _ -> pure ()

take :: forall seq m. (MonadFail m, MonadState seq m, IsSequence seq) => Int -> m [Element seq]
take i = if i <= 0
  then pure mempty
  else (:) <$> uncons' <*> take (i - 1)
  where
  uncons' :: m (Element seq)
  uncons' = get >>= uncons >>> maybe e pure >>= \(x,y) -> x <$ put y
  e = fail $ "End of seq while still needeing to take " <> show i <> " elements."

read :: forall a mono m. (Read a, Typeable a, MonoFoldable mono, ToChar (Element mono), MonadFail m) => mono -> m a
read = toString >>> readMay >>> maybe e pure
  where
  e = fail $ "Failed to read: " <> showsTypeRep (typeRep (Proxy @a)) mempty
  toString = ofoldr (\x xs -> toChar x : xs) mempty

