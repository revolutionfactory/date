module Date.SerializeSpec where

import Date.Types
import Date.Arbitrary
import qualified Test.Hspec.SmallCheck as SC
import Test.Hspec
import Test.QuickCheck
import Date.Serialize
import Control.Newtype (unpack)


spec :: Spec
spec = do
  describe "date" do
    it "always has eight digits" $ property \(DateYearFourDigit d) -> serialize d mempty `shouldSatisfy` (== 8) . length
    it "20201112" $ serialize Date {year=2020, month=November, day=12::Day} mempty `shouldBe` "20201112"

  describe "year" do
    it "always has four digits" $ property \(YearFourDigit y) -> serialize y mempty `shouldSatisfy` (== 4) . length
    it "reads equivalent" $ property \(YearFourDigit y) -> read (serialize y mempty) `shouldBe` unpack y

  describe "month" do
    it "always has two digits" $ SC.property \(MonthA m) -> serialize m mempty `shouldSatisfy` (== 2) . length
    it "adds one to enum int" $ SC.property \(MonthA m) -> read (serialize m mempty) `shouldBe` fromEnum m + 1

  describe "day" do
    it "always has two digits" $ SC.property \(DayA (d :: Day)) -> serialize d mempty `shouldSatisfy` (== 2) . length
    it "reads equivalent" $ SC.property \(DayA (d :: Day)) -> read (serialize d mempty) `shouldBe` unpack d

