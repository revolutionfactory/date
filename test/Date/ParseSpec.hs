module Date.ParseSpec where

import Test.Hspec
import Test.QuickCheck
import Date.Parse
import Date.Serialize
import Date.Arbitrary
import Date.Types


spec :: Spec
spec = do
  describe "Date" do
    it "parse isomorphic with serialize" $ property \(DateYearFourDigit d) ->
      parse (serialize d mempty) `shouldBe` (pure d :: Parse Date)

  describe "Year" do
    it "2020" $ parse "2020" `shouldBe` (pure 2020 :: Parse Year)

  describe "Month" do
    it "January" $ parse "01" `shouldBe` (pure January :: Parse Month)
    it "December" $ parse "12" `shouldBe` (pure December :: Parse Month)

  describe "Day" do
    it "02" $ parse "02" `shouldBe` (pure 2 :: Parse Day)
