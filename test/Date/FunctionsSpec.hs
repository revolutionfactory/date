module Date.FunctionsSpec where

import Test.QuickCheck.Instances.Natural ()
import Date.Arbitrary
import Date.Functions
import Date.Types
import Test.Hspec
import qualified Test.Hspec.SmallCheck as SC
import Test.QuickCheck


spec :: Spec
spec = do
  describe "daysInMonth" do
    describe "leap year" do
      let f :: Integer -> Int
          f = daysInMonth . (,February) . Year

      describe "is" do
        let leapYear = (`shouldBe` 29) . f

        it "2000" $ leapYear 2000
        it "400" $ property $ leapYear . (* 400)
        it "4 !100" $ forAll (suchThat ((* 4) <$> arbitrary) $ not . divisableBy oneHundred) leapYear

      describe "not" do
        let notLeapYear = (`shouldBe` 28) . f

        it "1900" $ notLeapYear 1900
        it "100 !400" $ forAll (suchThat ((* 100) <$> arbitrary) $ not . divisableBy fourHundred) notLeapYear

  it "decrementMonth" $ property \(DateA (d :: DateX Int)) -> decrementMonth d `shouldSatisfy` (< d)
  it "incrementMonth" $ property \(DateA (d :: DateX Int)) -> incrementMonth d `shouldSatisfy` (> d)

  describe "addDays" do
    it "reversable" $ property \(NonZero x, DateEnum d) -> do
      let d' = addDays x d
      d' `shouldNotBe` d
      addDays (negate x) d' `shouldBe` d

    describe "positive" do
      it "> 0" $ property \(Positive x, DateEnum d) -> addDays x d `shouldSatisfy` (> d)

      it "increment month" $ property \(DateMaxDay d) -> do
        let d' = addDays 1 d
        month d' `shouldBe` cycleNext (month d)
        day d' `shouldBe` 1
        d' `shouldSatisfy` (> d)

    describe "negative" do
      it "< 0" $ property \(Negative x, DateEnum d) -> addDays x d `shouldSatisfy` (< d)

      it "decrement month" $ property \(DateFirst d) -> do
        let d' = addDays (-1) d
        month d' `shouldBe` cyclePrev (month d)
        day d' `shouldBe` daysInMonth d'
        d' `shouldSatisfy` (< d)

  describe "normalizeDate" do
    it "is identity if day is within bounds" $ property \(DateA d) -> normalizeDate d `shouldBe` d

    it "increments month when day is greater than number of days in month" $ SC.property \(MonthA m) -> do
      let year = 0
      let d = normalizeDate Date {year, month=m, day = daysInMonth (year,m) + 1}
      month d `shouldBe` cycleNext m

    it "incremented day lands on the first" $ SC.property \(MonthA m) -> do
      let year = 0
      let d = normalizeDate Date {year, month=m, day = daysInMonth (year,m) + 1}
      day d `shouldBe` 1

    it "decrements month when day is zero" $ SC.property \(MonthA m) -> do
      let d = Date {year=0, month=m, day=0}
      let d' = normalizeDate d
      let m' = cyclePrev $ month d
      month d' `shouldBe` m'
      month d' `shouldNotBe` month d

    it "decramented month has correct number of days" $ SC.property \(MonthA m) -> do
      let d = Date {year=0, month=m, day=0}
      let d' = normalizeDate d
      let m' = cyclePrev $ month d
      day d' `shouldBe` daysInMonth (year d', m')

  describe "decrementMonth" do
    it "month decreased" $ SC.property \(MonthA m) -> do
      let d = Date {year=0, month=m, day = Day ()}
      let d' = decrementMonth d
      month d `shouldNotBe` month d'

  describe "normalizeDate" do
    it "good date not changed" $ property \(DateA d) -> normalizeDate d `shouldBe` d

