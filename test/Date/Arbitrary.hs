module Date.Arbitrary where

import Control.Newtype (Newtype(pack,unpack))
import Data.Coerce (Coercible, coerce)
import Date.Functions (daysInMonth)
import Date.Types
import GHC.Generics (Generic)
import System.Random (Random)
import Test.QuickCheck
import Test.QuickCheck.Instances.Natural ()
import qualified Test.SmallCheck.Series as SC


newtype DateA a = DateA (DateX a) deriving (Show)
instance Newtype (DateA a) (DateX a)
instance (Num a, Random a, Arbitrary a, Ord a) => Arbitrary (DateA a) where
  arbitrary = arbitraryDate choose
  shrink = shrinkDate

newtype DateEnum a = DateEnum (DateX a) deriving (Show)
instance Newtype (DateEnum a) (DateX a)
instance (Num a, Enum a, Arbitrary a, Ord a) => Arbitrary (DateEnum a) where
  arbitrary = arbitraryDate chooseEnum
  shrink = shrinkDate

arbitraryDate :: (Newtype (a b) (DateX b), Num b) => ((b,b) -> Gen b) -> Gen (a b)
arbitraryDate choose_ = do
  YearA year <- arbitrary
  MonthA month <- arbitrary
  day <- Day <$> choose_ (1, daysInMonth (year,month))
  pure $ pack $ Date {..}

newtype DateYearFourDigit = DateYearFourDigit Date deriving (Show)
instance Newtype DateYearFourDigit Date
instance Arbitrary DateYearFourDigit where
  arbitrary = do
    YearFourDigit year <- arbitrary
    MonthA month <- arbitrary
    day <- Day <$> chooseEnum (1, daysInMonth (year,month))
    pure $ pack $ Date {..}
  shrink = shrinkDate

-- shrinkDate :: (Newtype (a b) (DateX b), Coercible (a b) (DateX b), Arbitrary b, Ord b, Num b) => a b -> [a b]
shrinkDate :: (Newtype a (DateX b), Coercible a (DateX b), Arbitrary b, Ord b, Num b) => a -> [a]
shrinkDate =
  coerce .
  filter (\d -> daysInMonth d >= day d) .
  fmap (\(YearA year, MonthA month, day) -> Date {year, month, day = Day day}) .
  shrink .
  (\(Date {year,month,day=(Day day)}) -> (YearA year, MonthA month, day)) .
  unpack


newtype DateMaxDay = DateMaxDay Date deriving (Show)
instance Newtype DateMaxDay Date
instance Arbitrary DateMaxDay where
  arbitrary = do
    YearA year <- arbitrary
    MonthA month <- arbitrary
    let day = daysInMonth (year,month)
    pure $ pack $ Date {..}
  shrink = shrinkYearMonth
  -- shrink (DateMaxDay (Date {year,month,day})) =
  --   DateMaxDay .
  --   (\(YearA y, MonthA m) -> Date {year = y, month = m, day}) <$>
  --   shrink (YearA year, MonthA month)

newtype DateFirst = DateFirst Date deriving (Show)
instance Newtype DateFirst Date
instance Arbitrary DateFirst where
  arbitrary = do
    YearA year <- arbitrary
    MonthA month <- arbitrary
    let day = 1
    pure $ pack $ Date {..}
  shrink = shrinkYearMonth

shrinkYearMonth :: Newtype d (DateX a) => d -> [d]
shrinkYearMonth = 
  (\(Date {year,month,day}) -> pack . mkDate day <$> shrink (YearA year, MonthA month)) .
  unpack
  where
  mkDate day (YearA year, MonthA month) = Date {year,month,day}
  
newtype YearA = YearA Year deriving (Show, Generic)
instance Newtype YearA Year
instance Arbitrary YearA where
  arbitrary = YearA . Year <$> arbitrary
  shrink = shrinkYear

newtype YearFourDigit = YearFourDigit Year deriving (Show, Generic)
instance Newtype YearFourDigit Year
instance Arbitrary YearFourDigit where
  arbitrary = YearFourDigit . Year <$> choose (0,9999)
  shrink = shrinkYear

shrinkYear :: (Newtype a Year, Coercible a Year) => a -> [a]
shrinkYear = coerce . shrink . unpack . unpack

newtype MonthA = MonthA Month deriving (Show, Generic)
instance Arbitrary MonthA where
  arbitrary = MonthA <$> arbitraryBoundedEnum
  shrink (MonthA x) = coerce $ genericShrink x
instance Monad m => SC.Serial m MonthA where
  series = MonthA <$> SC.genericSeries
  
newtype DayA = DayA Day deriving (Show, Num, Enum)
instance Monad m => SC.Serial m DayA where series = SC.generate $ const [1..31]

