module Date.NowSpec where

import Test.Hspec
import Date.Parse
import Date.Types
import Date.Now


spec :: Spec
spec = do
  it "2020 11 12\\n" $ parse "2020 11 12\n" `shouldBe`
    (pure $ DateSpace $ Date {year=2020, month=November, day=12} :: Parse DateSpace)

  it "now" $ now >>= (`shouldSatisfy` (>= Date {year=2020,month=November,day=12}))

